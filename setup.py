# Copyright (C) 2018 Catalyst IT Ltd
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.

from setuptools import setup, find_packages

with open('requirements.txt') as f:
    required = f.readlines()

with open('README.rst') as file:
    long_description = file.read()

setup(
    name='openstack-checks',
    version='0.2.0',
    description='',
    long_description=long_description,
    url='',
    author='Adrian Turjak',
    author_email='adriant@catalyst.net.nz',
    license='Apache 2.0',
    classifiers=[
        'Development Status :: 5 - Production/Stable',
        'Intended Audience :: Developers',
        'Intended Audience :: System Administrators',
        'License :: OSI Approved :: Apache Software License',
        'Framework :: Django :: 1.11',
        'Programming Language :: Python :: 2.7',
        'Programming Language :: Python :: 3.5',
    ],
    keywords='',
    packages=find_packages(),
    install_requires=required,
    entry_points={
        'console_scripts': [
            ('check-ceilometer-samples '
             '= openstack_checks.check_ceilometer_samples:run'),
            ('check-ceilometer-hypervisor-samples '
             '= openstack_checks.check_ceilometer_hypervisor_samples:run'),
            'check-distil = openstack_checks.check_distil:run',
            'check-glance = openstack_checks.check_glance:run',
            'check-keystone = openstack_checks.check_keystone:run',
            ('check-s3-compatibility = '
             'openstack_checks.check_s3_compatibility:run'),
            'check-roles = openstack_checks.check_roles:run',
            'check-credentials = openstack_checks.check_credentials:run',
            'check-mfa = openstack_checks.check_mfa:run',
            'check-k8s-clusterhealth = openstack_checks.check_k8s_cluster_health:run',
            # deprecated name:
            ('check-ceilometer-compute-agent '
             '= openstack_checks.check_ceilometer_hypervisor_samples:run'),
        ],
    }
)
