OpenStack Checks
================

Some basic python checks for OpenStack which can be used for monitoring.

Installation
++++++++++++

This is a python library that can be installed and run as commands
in your python environment.

The safest place for these is in a python virtual environment.

:: code-block::

  # first clone the repo
  git clone <this repo>

  # now make a venv
  virtualenv checks_env

  # enter your venv
  source checks_env/bin/activate

  # install the library
  pip install openstack-checks/

  # now run one of the checks
  check-keystone

  # or if you want to see some help for each check
  check-keystone -h

  # or once outside the venv, you can run the commands directly
  checks_env/bin/check-keystone

Check Commands
++++++++++++++

The commands are as follows:

* ``check-ceilometer-samples``
* ``check-ceilometer-compute-agent``
* ``check-distil``
* ``check-glance``
* ``check-keystone``
* ``check-s3-compatibility``
* ``check-roles``

Check Roles
-----------

Requires a yaml file in the structure::

    <role1>:
        - <expected_user1>
        - <expected_user2>
        - <expected_user3>
    <role2>:
        - <expected_user4>
    # null means none expected
    <role3>: null

Check Credentials
-----------------

Requires a yaml file in the structure::

    <role1>:
        <credential_type1>:
            - <exempt_user1>
            - <exempt_user2>
        # null means no exemptions
        <credential_type2>: null
    <role2>:
        <credential_type1>: null
