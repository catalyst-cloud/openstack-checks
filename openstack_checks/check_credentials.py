#!/usr/bin/env python
#
# Copyright (c) 2018 Catalyst.net Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may
# not use this file except in compliance with the License. You may obtain
# a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.

import yaml

from openstack_checks.shared import check


class CheckCredentials(check.BaseCheck):
    """
    Check that certain credentials exist for given roles.
    """

    def get_parser(self, parser):
        keystone_args = parser.add_argument_group('Credentials check')

        keystone_args.add_argument(
            'role_file', metavar='<role-file>',
            help='yaml file for roles and credentials to check.')

        keystone_args.add_argument(
            '--critical', action='store_true',
            help="Go critical if missing users. Only warns by default.",
            default=False
        )

    def take_action(self, parsed_args):
        with open(parsed_args.role_file) as f:
            roles_yaml = yaml.load(f)

        conn = self.get_connection()

        # NOTE(adriant): Until the sdk fixes query filters for role assignments
        # we have to use the keystoneclient.
        ks = self.get_client(service='identity')

        role_mapping = {r.name: r.id for r in conn.identity.roles()}
        users = list(conn.identity.users())
        user_mapping = {u.id: u for u in users}
        user_name_mapping = {u.id: u.name for u in users}

        users_by_role = {}
        users_by_credential = {}
        missing_per_role_per_cred = {}
        for role, credentials in roles_yaml.items():
            if role not in role_mapping:
                print("CRITCAL: '%s' is not a valid role name." % role)
                return 2

            # NOTE(adriant): for when the SDK is fixed:
            # users_by_role[role] = [
            #     a.user['name'] for a in
            #     conn.identity.role_assignments(
            #         role_id=role_mapping[role], include_names=True)
            #     if a.user
            # ]
            users_by_role[role] = {
                a.user['name'] for a in
                ks.role_assignments.list(
                    role=role_mapping[role], include_names=True)
                if a.user and user_mapping[a.user['id']].is_enabled
            }

            missing_per_role_per_cred[role] = {}
            for cred in credentials:
                if cred in users_by_credential:
                    continue
                users_by_credential[cred] = {
                    user_name_mapping[c.user_id] for c in
                    conn.identity.credentials(type=cred)
                }

                missing_users = users_by_role[role] - users_by_credential[cred]

                if roles_yaml[role][cred]:
                    missing_users = missing_users - set(roles_yaml[role][cred])

                missing_per_role_per_cred[role][cred] = missing_users

        error_level = "CRITCAL" if parsed_args.critical else "WARNING"
        missing = False
        for role, credentials in missing_per_role_per_cred.items():
            for cred, missing_users in credentials.items():
                if missing_users:
                    missing = True
                    str_missing = ', '.join(missing_users)
                    print("%s: User with role '%s' missing cred '%s': %s" %
                          (error_level, role, cred, str_missing))

        if missing and parsed_args.critical:
            return 2
        elif missing:
            return 1

        print("OK: All role appear to have expected credentials.")
        return 0


def run():
    CheckCredentials().run()


if __name__ == '__main__':
    run()
