#!/usr/bin/env python
#
# Copyright (c) 2018 Catalyst.net Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may
# not use this file except in compliance with the License. You may obtain
# a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.

import yaml

from openstack_checks.shared import check


class CheckRoles(check.BaseCheck):
    """
    Check that only the expected users have a give role.
    """

    def get_parser(self, parser):
        keystone_args = parser.add_argument_group('Role check')

        keystone_args.add_argument(
            'role_file', metavar='<role-file>',
            help='yaml file mapping roles to lists of users.')

    def take_action(self, parsed_args):
        with open(parsed_args.role_file) as f:
            roles_yaml = yaml.load(f)

        conn = self.get_connection()

        # NOTE(adriant): Until the sdk fixes query filters for role assignments
        # we have to use the keystoneclient.
        ks = self.get_client(service='identity')

        role_mapping = {r.name: r.id for r in conn.identity.roles()}

        user_mapping = {u.id: u for u in conn.identity.users()}

        unexpected_per_role = {}
        missing_per_role = {}
        for role, expected in roles_yaml.items():
            if role not in role_mapping:
                print("CRITCAL: '%s' is not a valid role name." % role)
                return 2

            if expected:
                expected = set(expected)
            else:
                expected = set()

            # NOTE(adriant): for when the SDK is fixed:
            # role_users = {
            #     a.user['name'] for a in conn.identity.role_assignments(
            #         role_id=role_mapping[role], include_names=True)
            #     if a.user
            # }
            role_users = {
                a.user['name'] for a in ks.role_assignments.list(
                    role=role_mapping[role], include_names=True)
                if a.user and user_mapping[a.user['id']].is_enabled
            }

            unexpected = role_users - expected
            if unexpected:
                unexpected_per_role[role] = unexpected
            missing = expected - role_users
            if missing:
                missing_per_role[role] = missing

        warn = False
        if missing_per_role:
            warn = True
            for role, missing in missing_per_role.items():
                str_missing = ', '.join(missing)
                print("WARNING: role '%s' is missing users: %s" %
                      (role, str_missing))

        crit = False
        if unexpected_per_role:
            crit = True
            for role, unexpected in unexpected_per_role.items():
                str_unexpected = ', '.join(unexpected)
                print("CRITICAL: role '%s' has unexpected users: %s" %
                      (role, str_unexpected))

        if crit:
            return 2
        if warn:
            return 1

        print("OK: All roles appear to be as expected.")
        return 0


def run():
    CheckRoles().run()


if __name__ == '__main__':
    run()
