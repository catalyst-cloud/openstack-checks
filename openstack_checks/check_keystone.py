#!/usr/bin/env python
#
# Copyright (c) 2018 Catalyst.net Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may
# not use this file except in compliance with the License. You may obtain
# a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.

from collections import defaultdict

from openstack_checks.shared import check


class CheckKeystone(check.BaseCheck):
    """
    Check an OpenStack Keystone server.
    """

    def get_parser(self, parser):
        keystone_args = parser.add_argument_group('Keystone check')

        keystone_args.add_argument(
            'services', metavar='SERVICE', type=str, nargs='*',
            help='services to check for in the endpoint catalog')

    def take_action(self, parsed_args):
        try:
            conn = self.get_connection()
            # projects() returns a generator, so you need to actually trigger
            # the underlying API call.
            projects = list(conn.identity.projects()) # noqa
        except Exception as e:
            print(
                "CRITICAL - Failed to get projects from Keystone. "
                "Error: %s" % e)
            return 2

        try:
            endpoints = conn.identity.endpoints()
            service_endpoints = defaultdict(list)

            for endpoint in endpoints:
                service_endpoints[endpoint.service_id].append(endpoint)
        except Exception as e:
            print(
                "CRITICAL - Failed to get endpoints from Keystone. "
                "Error: %s" % e)
            return 2

        try:
            service_objs = list(conn.identity.services())
        except Exception as e:
            print(
                "CRITICAL - Failed to get services from Keystone. "
                "Error: %s" % e)
            return 2
        service_names = [s.name for s in service_objs]
        service__name_id_mapping = {s.name: s.id for s in service_objs}
        service_type_id_mapping = {s.type: s.id for s in service_objs}

        if parsed_args.services:
            expected_service_options = (
                service__name_id_mapping.keys() +
                service_type_id_mapping.keys())

            msgs = []
            for service in parsed_args.services:
                if service not in expected_service_options:
                    msgs.append(
                        "%s is not a valid service name or type." % service)

            if msgs:
                print("\n".join(msgs))
                return 2

            services = parsed_args.services or service_names
        else:
            services = service_names

        msgs = []
        for service in services:
            service_id = (
                service__name_id_mapping.get(service) or
                service_type_id_mapping.get(service))

            if service_id not in service_endpoints:
                msgs.append("`%s' service is missing from catalog" % service)
                continue

            has_public = False
            for endpoint in service_endpoints[service_id]:
                if endpoint.interface == "public":
                    has_public = True

            if not has_public:
                msgs.append("`%s' service has no publicURL" % service)

        if msgs:
            print("\n".join(msgs))
            return 2

        print("OK: Keystone returned expected service endpoints.")
        return 0


def run():
    CheckKeystone().run()


if __name__ == '__main__':
    run()
