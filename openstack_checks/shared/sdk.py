#
# Copyright (c) 2018 Catalyst.net Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may
# not use this file except in compliance with the License. You may obtain
# a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.

from openstack import connection

from openstack_checks.shared import defaults


DEFAULT_KWARGS = {
    'compute_api_version': defaults.DEFAULT_COMPUTE_VERSION,
    'identity_api_version': defaults.DEFAULT_IDENTITY_VERSION,
    'image_api_version': defaults.DEFAULT_IMAGE_VERSION,
    'network_api_version': defaults.DEFAULT_NETWORK_VERSION,
    'object_store_api_version': defaults.DEFAULT_OBJECT_STORAGE_VERSION,
    'orchestration_api_version': defaults.DEFAULT_ORCHESTRATION_VERSION,
    'volume_api_version': defaults.DEFAULT_VOLUME_VERSION,
    'rating_api_version': defaults.DEFAULT_RATING_VERSION,
    'container_infra_api_version': defaults.DEFAULT_CONTAINER_INFRA_VERSION,
}


def get_sdk_connection(session, **kwargs):
    new_kwargs = dict(DEFAULT_KWARGS)
    new_kwargs.update(kwargs)

    return connection.Connection(session=session, **new_kwargs)
