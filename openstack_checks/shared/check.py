#
# Copyright (c) 2018 Catalyst.net Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may
# not use this file except in compliance with the License. You may obtain
# a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.

import argparse
from collections import OrderedDict
from uuid import uuid4
import sys
import traceback

from openstack_checks.shared import auth
from openstack_checks.shared.openstack_clients import get_openstack_client

from openstack_checks.shared.sdk import get_sdk_connection


class BaseCheck(object):

    keystone_auth = True

    def get_parser(self, parser):
        return parser

    def take_action(self, parsed_args):
        raise NotImplementedError()

    def get_base_parser(self):

        parser = argparse.ArgumentParser(description=self.__doc__)

        if self.keystone_auth:
            auth.add_identity_args(parser)
        self.get_parser(parser)

        return parser

    def get_client(self, service, region=None, version=None):
        if self.keystone_auth:
            region = region or self.args.os_region_name
            return get_openstack_client(
                service, self.session, region, version)
        return None

    def get_connection(self, region=None, **kwargs):
        if self.keystone_auth:
            region = region or self.args.os_region_name
            return get_sdk_connection(
                session=self.session, region=region, **kwargs)
        return None

    def add_cleanup(self, function, *args, **kwargs):
        clean_action_id = uuid4().hex
        self._to_cleanup[clean_action_id] = {
            "function": function,
            "args": args,
            "kwargs": kwargs,
        }
        return clean_action_id

    def remove_cleanup(self, clean_action_id):
        self._to_cleanup.pop(clean_action_id)

    def run_cleanup(self):
        for cleanup in reversed(self._to_cleanup.values()):
            try:
                cleanup['function'](*cleanup['args'], **cleanup['kwargs'])
            except Exception as e:
                print(
                    "Cleanup failed for function: %s with "
                    "args: %s and kwargs: %s and got error: %s" % (
                        cleanup['function'], cleanup['args'],
                        cleanup['kwargs'], e))

    def run(self):
        try:
            parser = self.get_base_parser()
            self.args = parser.parse_args()
            if self.keystone_auth:
                self.auth, self.session = auth.authenticate(self.args)
            self._to_cleanup = OrderedDict()
            exitcode = self.take_action(self.args)
            self.run_cleanup()
            sys.exit(exitcode)
        except Exception as e:
            self.run_cleanup()
            traceback.print_exc()
            print("CRITICAL: Unknown generic failure: %s" % e)
            sys.exit(2)
