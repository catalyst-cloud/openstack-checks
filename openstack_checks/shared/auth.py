#
# Copyright (c) 2018 Catalyst.net Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may
# not use this file except in compliance with the License. You may obtain
# a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.

import os

from keystoneauth1.identity import generic
from keystoneauth1 import session as ksession


def add_identity_args(parser):

    parser.add_argument(
        '-k', '--insecure',
        default=False,
        action='store_true',
        help=(
            'Explicitly allow this client to perform '
            '"insecure SSL" (https) requests. The server\'s '
            'certificate will not be verified against any '
            'certificate authorities. This option should '
            'be used with caution.'))

    parser.add_argument(
        '--os-cert',
        help=(
            'Path of certificate file to use in SSL '
            'connection. This file can optionally be '
            'prepended with the private key.'))

    parser.add_argument(
        '--os-key',
        help=(
            'Path of client key to use in SSL '
            'connection. This option is not necessary '
            'if your key is prepended to your cert file.'))

    parser.add_argument(
        '--os-cacert',
        metavar='<ca-certificate-file>',
        dest='os_cacert',
        default=os.environ.get('OS_CACERT'),
        help=(
            'Path of CA TLS certificate(s) used to '
            'verify the remote server\'s certificate. '
            'Without this option glance looks for the '
            'default system CA certificates.'))

    parser.add_argument(
        '--os-username',
        default=os.environ.get('OS_USERNAME'),
        help='Defaults to env[OS_USERNAME]')

    parser.add_argument(
        '--os-password',
        default=os.environ.get('OS_PASSWORD'),
        help='Defaults to env[OS_PASSWORD]')

    parser.add_argument(
        '--os-project-id',
        default=os.environ.get(
            'OS_PROJECT_ID', os.environ.get(
                'OS_TENANT_ID')),
        help='Defaults to env[OS_PROJECT_ID]')

    parser.add_argument(
        '--os-project-name',
        default=os.environ.get(
            'OS_PROJECT_NAME', os.environ.get(
                'OS_TENANT_NAME')),
        help='Defaults to env[OS_PROJECT_NAME]')

    parser.add_argument(
        '--os-project-domain-id',
        default=os.environ.get('OS_PROJECT_DOMAIN_ID'),
        help='Defaults to env[OS_PROJECT_DOMAIN_ID]')

    parser.add_argument(
        '--os-project-domain-name',
        default=os.environ.get('OS_PROJECT_DOMAIN_NAME'),
        help='Defaults to env[OS_PROJECT_DOMAIN_NAME]')

    parser.add_argument(
        '--os-user-domain-id',
        default=os.environ.get('OS_USER_DOMAIN_ID'),
        help='Defaults to env[OS_USER_DOMAIN_ID]')

    parser.add_argument(
        '--os-user-domain-name',
        default=os.environ.get('OS_USER_DOMAIN_NAME'),
        help='Defaults to env[OS_USER_DOMAIN_NAME]')

    parser.add_argument(
        '--os-auth-url',
        default=os.environ.get('OS_AUTH_URL'),
        help='Defaults to env[OS_AUTH_URL]')

    parser.add_argument(
        '--os-region-name',
        default=os.environ.get('OS_REGION_NAME'),
        help='Defaults to env[OS_REGION_NAME]')

    parser.add_argument(
        '--os-token',
        default=os.environ.get('OS_TOKEN'),
        help='Defaults to env[OS_TOKEN]')


def authenticate(args):
    if args.insecure:
        verify = False
    else:
        verify = args.os_cacert or True
    if args.os_cert and args.os_key:
        cert = (args.os_cert, args.os_key)
    else:
        cert = None

    if args.os_token:
        kwargs = {
            'token': args.os_token,
            'auth_url': args.os_auth_url,
            'project_id': args.os_project_id,
            'project_name': args.os_project_name,
            'project_domain_id': args.os_project_domain_id,
            'project_domain_name': args.os_project_domain_name,
        }
        auth = generic.Token(**kwargs)
        session = ksession.Session(auth=auth, verify=verify, cert=cert)
    else:
        kwargs = {
            'username': args.os_username,
            'password': args.os_password,
            'auth_url': args.os_auth_url,
            'project_id': args.os_project_id,
            'project_name': args.os_project_name,
            'project_domain_id': args.os_project_domain_id,
            'project_domain_name': args.os_project_domain_name,
            'user_domain_id': args.os_user_domain_id,
            'user_domain_name': args.os_user_domain_name,
        }
        auth = generic.Password(**kwargs)
        session = ksession.Session(auth=auth, verify=verify, cert=cert)
    return auth, session


def ensure_url_version(url, version):
    if version in [3, "3", "v3"]:
        if url.endswith('v3'):
            return url
        elif url.endswith('v2.0'):
            return url.replace("v2.0", "v3")
        elif url.endswith('/'):
            return url + "v3"
        else:
            return url + "/v3"
    elif version in [2, "2", "2.0", "v2", "v2.0"]:
        if url.endswith('v2'):
            return url
        elif url.endswith('v3'):
            return url.replace("v3", "v2.0")
        elif url.endswith('/'):
            return url + "v2.0"
        else:
            return url + "/v2.0"
    else:
        return url
