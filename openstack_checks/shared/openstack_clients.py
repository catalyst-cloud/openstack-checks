#
# Copyright (c) 2018 Catalyst.net Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may
# not use this file except in compliance with the License. You may obtain
# a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.

from ceilometerclient import client as ceilometerclient
from cinderclient import client as cinderclient
from distilclient import client as distilclient
from glanceclient import client as glanceclient
from heatclient import client as heatclient
from keystoneclient import client as keystoneclient
from magnumclient import client as magnumclient
from neutronclient.v2_0 import client as neutronclient
from novaclient import client as novaclient

from openstack_checks.shared import defaults


class ServiceNotFound(Exception):
    """Service not found."""


def get_openstack_client(service, session, region, version=None):
    if service == 'compute':
        return novaclient.Client(
            version or defaults.DEFAULT_COMPUTE_VERSION,
            session=session, region_name=region)
    if service == 'identity':
        return keystoneclient.Client(
            version or defaults.DEFAULT_IDENTITY_VERSION,
            session=session, region_name=region)
    if service == 'image':
        return glanceclient.Client(
            version or defaults.DEFAULT_IMAGE_VERSION,
            session=session, region_name=region)
    if service == 'metering':
        return ceilometerclient.Client(
            version or defaults.DEFAULT_METERING_VERSION,
            session=session, region_name=region)
    if service == 'network':
        return neutronclient.Client(
            session=session, region_name=region)
    if service == 'orchestration':
        return heatclient.Client(
            version or defaults.DEFAULT_ORCHESTRATION_VERSION,
            session=session, region_name=region)
    if service == 'volume':
        return cinderclient.Client(
            version or defaults.DEFAULT_VOLUME_VERSION,
            session=session, region_name=region)
    if service == 'ratingv2':
        return distilclient.Client(
            version=version or defaults.DEFAULT_RATING_VERSION,
            service_type=service,
            session=session,
            region_name=region
        )
    if service == 'container-infra':
        return magnumclient.Client(
            version or defaults.DEFAULT_CONTAINER_INFRA_VERSION,
            session=session, region_name=region)

    raise ServiceNotFound
