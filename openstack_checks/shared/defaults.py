#
# Copyright (c) 2018 Catalyst.net Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may
# not use this file except in compliance with the License. You may obtain
# a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.


# Default API versions:

DEFAULT_COMPUTE_VERSION = "2"

DEFAULT_IDENTITY_VERSION = "3"

DEFAULT_IMAGE_VERSION = "2"

DEFAULT_CONTAINER_INFRA_VERSION = "1"

DEFAULT_METERING_VERSION = "2"

DEFAULT_NETWORK_VERSION = "2"

DEFAULT_OBJECT_STORAGE_VERSION = "1"

DEFAULT_ORCHESTRATION_VERSION = "1"

DEFAULT_VOLUME_VERSION = "2"

DEFAULT_RATING_VERSION = "2"
