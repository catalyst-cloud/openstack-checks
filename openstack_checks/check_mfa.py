#!/usr/bin/env python
#
# Copyright (c) 2018 Catalyst.net Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may
# not use this file except in compliance with the License. You may obtain
# a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.

import base64
import os
import time
import random
from uuid import uuid4

from keystoneauth1.identity import v3, v2
from keystoneauth1 import session
from keystoneauth1.exceptions import Unauthorized

from openstack_checks.shared import check, totp
from openstack_checks.shared import auth as auth_utils


class CheckMFA(check.BaseCheck):
    """
    Check that MFA works as expected on a cloud using Adjutant-MFA.
    """

    def get_parser(self, parser):
        keystone_args = parser.add_argument_group('MFA check')

        keystone_args.add_argument(
            '--role', default='_member_',
            help='Role used when testing user auth. Default: _member_')

        keystone_args.add_argument(
            '--user-prefix',
            default='mfa-test-user',
            help='Name for the test user created for this check. '
                 'Default: mfa-test-user')

        keystone_args.add_argument(
            '--project-prefix',
            default='mfa-test-project',
            help='Name for the test project created for this check. '
                 'Default: mfa-test-project')

        keystone_args.add_argument(
            '--delay', type=int, default=0,
            help='Number of seconds to delay start of check. '
                 'Negative number (e.g. -1) forces a random delay '
                 'between 0-30 seconds. Default: 0')

    def take_action(self, parsed_args):
        delay = parsed_args.delay
        if delay < 0:
            delay = random.randrange(30)
        time.sleep(delay)

        conn = self.get_connection()

        try:
            role = list(conn.identity.roles(name=parsed_args.role))[0]
        except IndexError:
            print("CRITICAL - %s is not a valid role." % parsed_args.role)
            return 2

        test_user_password = uuid4().hex
        test_user = conn.identity.create_user(
            name="%s~%s" % (parsed_args.user_prefix, uuid4().hex),
            password=test_user_password)
        self.add_cleanup(test_user.delete, conn.identity)
        # sleep after user creation:
        time.sleep(3)

        test_project = conn.identity.create_project(
            name="%s~%s" % (parsed_args.project_prefix, uuid4().hex),
            description="Project used by the nagios MFA auth check.")
        self.add_cleanup(test_project.delete, conn.identity)
        # sleep after project creation:
        time.sleep(3)

        # add role on project
        test_project.assign_role_to_user(conn.identity, test_user, role)
        # sleep after role assignment:
        time.sleep(3)

        v3_auth_args = {
            "auth_url": auth_utils.ensure_url_version(
                conn.session.auth.auth_url, 3),
            "username": test_user.name,
            "project_name": test_project.name,
            "user_domain_id": 'default',
            "project_domain_id": 'default'
        }

        v2_auth_args = {
            "auth_url": auth_utils.ensure_url_version(
                conn.session.auth.auth_url, 2),
            "username": test_user.name,
            "tenant_name": test_project.name
        }

        auth = v3.Password(
            password=test_user_password, **v3_auth_args)
        sess = session.Session(auth=auth, verify=not parsed_args.insecure)

        try:
            sess.get_token()
        except Unauthorized as e:
            print("CRITICAL: User without MFA unable to auth to v3. "
                  "Error: %s" % e)
            return 2

        # add totp cred for user
        secret = base64.b32encode(os.urandom(20)).decode('utf-8')

        test_credential = conn.identity.create_credential(
            user_id=test_user.id, blob=secret, type="totp")
        cred_clean_id = self.add_cleanup(test_credential.delete, conn.identity)
        # sleep after cred creation:
        time.sleep(3)

        auth = v3.Password(
            password=test_user_password, **v3_auth_args)
        sess = session.Session(auth=auth, verify=not parsed_args.insecure)

        try:
            sess.get_token()
            # this should fail
            print("CRITICAL: User with MFA could auth without passcode.")
            return 2
        except Unauthorized:
            pass

        auth = v2.Password(
            password=test_user_password, **v2_auth_args)
        sess = session.Session(auth=auth, verify=not parsed_args.insecure)

        try:
            sess.get_token()
            # this should fail
            print("CRITICAL: User with MFA could auth to v2 without passcode.")
            return 2
        except Unauthorized:
            pass

        auth = v3.Password(
            password=test_user_password + totp.generate_totp_passcode(secret),
            **v3_auth_args)
        sess = session.Session(auth=auth, verify=not parsed_args.insecure)

        try:
            sess.get_token()
        except Unauthorized as e:
            print("CRITICAL: User with MFA unable to auth with passcode. "
                  "Error: %s" % e)
            return 2

        auth = v2.Password(
            password=test_user_password + totp.generate_totp_passcode(secret),
            **v2_auth_args)
        sess = session.Session(auth=auth, verify=not parsed_args.insecure)

        try:
            sess.get_token()
            # this should fail
            print("CRITICAL: User with MFA could auth to v2 with passcode.")
            return 2
        except Unauthorized:
            pass

        test_credential.delete(conn.identity)
        self.remove_cleanup(cred_clean_id)
        # sleep after cred deletion:
        time.sleep(3)

        auth = v3.Password(
            password=test_user_password, **v3_auth_args)
        sess = session.Session(auth=auth, verify=not parsed_args.insecure)

        try:
            sess.get_token()
        except Unauthorized as e:
            print("CRITICAL: User unable to auth after removing totp. "
                  "Error: %s" % e)
            return 2

        print("OK: MFA appears to work as expected.")
        return 0


def run():
    CheckMFA().run()


if __name__ == '__main__':
    run()
