#!/usr/bin/env python
#
# Copyright (c) 2018 Catalyst.net Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may
# not use this file except in compliance with the License. You may obtain
# a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.

from datetime import datetime, timedelta

from openstack_checks.shared import check


class CheckCeilometerSamples(check.BaseCheck):
    """
    Checks for ceilometer samples.
    """

    def get_parser(self, parser):
        ceilometer_args = parser.add_argument_group('Ceilometer check')

        ceilometer_args.add_argument(
            '--meters', nargs='*', default=['instance'],
            help='Meters to query (Defaults to just: instance).')

        ceilometer_args.add_argument(
            '--search-range', type=int, default=11,
            help='Search range, in minutes.')

        ceilometer_args.add_argument(
            '-w', '--warning', type=int, default=5,
            help='Warning theshold for number of samples.')

        ceilometer_args.add_argument(
            '-c', '--critical', type=int, default=0,
            help='Critical theshold for number of samples.')

    def take_action(self, parsed_args):
        ceilometer = self.get_client('metering')

        end = datetime.utcnow()
        start = end - timedelta(minutes=parsed_args.search_range)

        errors = {}
        warnings = {}
        for meter in parsed_args.meters:
            fields = {
                'meter_name': meter,
                'q': [
                    {'field': 'timestamp', 'op': 'ge', 'value': str(start)},
                    {'field': 'timestamp', 'op': 'lt', 'value': str(end)}
                ],
            }

            try:
                samples = ceilometer.samples.list(**fields)
            except Exception as e:
                errors[meter] = (
                    "unable to get samples from ceilometer. "
                    "Error: %s" % e)
                continue
            num_samples = len(samples)

            if num_samples <= parsed_args.critical:
                errors[meter] = "no recent samples."
                continue
            elif num_samples < parsed_args.warning:
                warnings[meter] = "Sample count below threshold."
                continue

        if warnings:
            for meter, warning in warnings.items():
                print("WARNING - %s: %s" % (meter, warning))

        if errors:
            for meter, error in errors.items():
                print("CRITICAL - %s: %s" % (meter, error))
            return 2

        if warnings:
            return 1

        print("OK: Samples collected.")
        return 0


def run():
    CheckCeilometerSamples().run()


if __name__ == '__main__':
    run()
