#!/usr/bin/env python
#
# Copyright (c) 2019 Catalyst.net Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may
# not use this file except in compliance with the License. You may obtain
# a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.

from openstack_checks.shared import check


class CheckK8sClusterHealth(check.BaseCheck):
    """
    Checks for k8s cluster health for all customers.
    """

    def get_parser(self, parser):
        magnum_args = parser.add_argument_group('Kubernetes check')

        magnum_args.add_argument(
            '-cs', '--check-status', type=list,
            default=["CREATE_COMPLETE", "UPDATE_COMPLETE"],
            help='Only check cluster in status which listed.')

    def take_action(self, parsed_args):
        magnum = self.get_client('container-infra')

        clusters = magnum.clusters.list()

        errors = {}
        warnings = {}
        for c in clusters:
            if c.status in parsed_args.check_status:
                if c.health_status != 'HEALTHY':
                    cluster = magnum.clusters.get(c.uuid)
                    if cluster.health_status_reason["api"] != "ok":
                        errors[cluster.uuid] = cluster
                    else:
                        warnings[cluster.uuid] = cluster

        if errors:
            print("CRITICAL - %s clusters in error state." % len(errors))
            for uuid, cluster in errors.items():
                print(
                    "CRITICAL - Cluster '%s' from project '%s' "
                    "with health reason: %s" %
                    (uuid, cluster.project_id, cluster.health_status_reason))

        # NOTE(flwang): The failure of worker nodes should be fixed by auto
        # healing if user enabled, otherwise, let's deal with the master
        # failure if there is any.
        if warnings:
            print("WARNING - %s clusters in warning state." % len(warnings))
            for uuid, cluster in warnings.items():
                print(
                    "WARNING - Cluster '%s' from project '%s' "
                    "with health reason: %s" %
                    (uuid, cluster.project_id, cluster.health_status_reason))

        if errors:
            return 2
        if warnings:
            return 1

        print("OK: All clusters are healthy.")
        return 0


def run():
    CheckK8sClusterHealth().run()


if __name__ == '__main__':
    run()
