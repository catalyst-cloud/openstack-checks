## Example Check template

```python
from openstack_checks.shared import check


class CheckExample(check.BaseCheck):
    """An example check that takes an error code, and exits on it.

    Not exactly very useful, but shows you all the pieces you need to
    make a working check.
    """

    # If you uncomment this, the check will not include keystone auth
    # arguments or attempt to auth:
    # keystone_auth = False

    def get_parser(self, parser):
        """
        The base class will call this function and pass along a parser
        object you can add your argument group to.
        """
        example_args = parser.add_argument_group('Stuff check')

        example_args.add_argument(
            '--exit-code', type=int, default=0, choices=[0, 1, 2],
            help='The code to exit on.')

    def take_action(self, parsed_args):
        """
        This function is where your check logic goes and should
        return a valid exit code as an int.

        Because this check by default includes keystone arguments you
        can get any openstack clients as follows:
            nova = self.get_client('compute')
            keystone = self.get_client('identity')

        or the sdk connection object with:
            conn = self.get_connection()
        """

        if parsed_args.exit_code == 0:
            print("OK: Example is all good.")

        if parsed_args.exit_code == 1:
            print("WARNING: Example is isn't entirely all good.")

        if parsed_args.exit_code == 2:
            print("CRITICAL: Example is going horribly wrong.")

        return parsed_args.exit_code


def run():
    CheckExample().run()


if __name__ == '__main__':
    run()

```
