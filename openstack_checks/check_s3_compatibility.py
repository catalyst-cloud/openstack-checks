#!/usr/bin/env python
#
# Copyright (c) 2018 Catalyst.net Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may
# not use this file except in compliance with the License. You may obtain
# a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.

import random

import boto
import boto.s3.connection

from openstack_checks.shared import check


class CheckS3Compatibility(check.BaseCheck):
    """
    Check run s3 compatibility checks.
    """

    def _get_rand_name(self, prefix=''):
        """Generate a random name that includes a random number

        :param str name: The name that you want to include
        :param str prefix: The prefix that you want to include
        :return: a random name. The format is
                 '<prefix>-<random number>-<name>-<random number>'.
                 (e.g. 'prefixfoo-1308607012-namebar-154876201')
        :rtype: string
        """
        randbits = str(random.randint(1, 0x7fffffff))
        rand_name = randbits
        if prefix:
            rand_name = prefix + '-' + rand_name
        return rand_name

    def _get_ec2_keys(self):
        keystone = self.get_client('identity')

        ec2_keys = keystone.ec2.list(self.session.get_user_id())

        if not ec2_keys:
            ec2_keys = [
                keystone.ec2.create(
                    self.session.get_user_id(),
                    self.session.get_project_id())
            ]

        for credential in ec2_keys:
            if credential.tenant_id == self.session.get_project_id():
                print credential.tenant_id
                return credential.access, credential.secret

    def get_parser(self, parser):
        s3_args = parser.add_argument_group('S3 check')

        s3_args.add_argument(
            '--host', dest='host', type=str, action='store',
            required=True, help='Swift proxy host to query')
        s3_args.add_argument(
            '--port', dest='port', type=int, action='store',
            default=8443, required=True,
            help='Port the Swift is listening to')

    def take_action(self, parsed_args):
        try:
            access_key, secret_access = self._get_ec2_keys()
        except Exception as e:
            print("CRITICAL: Failed to create ec2 key. ERROR: %s" % e)
            return 2

        try:
            conn = boto.connect_s3(
                aws_access_key_id=access_key,
                aws_secret_access_key=secret_access,
                host=parsed_args.host, port=parsed_args.port,
                calling_format=boto.s3.connection.OrdinaryCallingFormat()
            )
        except Exception as e:
            print("CRITICAL: Connect to boto. ERROR: %s" % e)
            return 2

        bucket_name = self._get_rand_name(prefix='nagios-check')

        bucket = None

        # create a test bucket
        try:
            bucket = conn.create_bucket(bucket_name)
        except Exception as e:
            print("CRITICAL: Failed to create bucket. ERROR: %s" % e)
            return 2

        # check for bucket availability
        try:
            bucket = conn.get_bucket(bucket_name)
            if bucket is None:
                print("CRITICAL: Failed to get bucket: %s" % bucket_name)
                return 2

            bucket.list()

            print("OK: Bucket operation successful.")
        except Exception as e:
            print("CRITICAL: Failed to retrieve test bucket: %s" % bucket_name)
            return 2
        finally:
            # Delete the bucket.
            if bucket:
                try:
                    conn.delete_bucket(bucket_name)
                except Exception as e:
                    # Ignore the 404 exception when deleting bucket.
                    if 'Not Found' != e.reason:
                        print("CRITICAL: Bucket operation failed, "
                              "reason: %s" % e.body)
                        return 2


def run():
    CheckS3Compatibility().run()


if __name__ == '__main__':
    run()
