#!/usr/bin/env python
#
# Copyright (c) 2018 Catalyst.net Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may
# not use this file except in compliance with the License. You may obtain
# a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.

from datetime import datetime, timedelta
from random import shuffle

from openstack_checks.shared import check


class CheckCeilometerHypervisorSamples(check.BaseCheck):
    """
    Check that instances on a given hypervisor are generating samples
    """

    def get_parser(self, parser):
        ceilometer_args = parser.add_argument_group('Hypervisor check')

        ceilometer_args.add_argument(
            '--hypervisor', required=True,
            help='Hostname of hypervisor to check')

        ceilometer_args.add_argument(
            '--search-range', type=int, default=60,
            help='Search range, in minutes.')

        ceilometer_args.add_argument(
            '--num-servers', type=int, default=3,
            help='How many servers to check.')

        ceilometer_args.add_argument(
            '-w', '--warning', type=int, default=5,
            help='Warning theshold for number of samples per server.')

        ceilometer_args.add_argument(
            '-c', '--critical', type=int, default=0,
            help='Critical theshold for number of samples per server..')

    def take_action(self, parsed_args):

        nova = self.get_client('compute')

        try:
            hypervisor = nova.hypervisors.search(
                parsed_args.hypervisor, servers=True)[0]
        except Exception as e:
            print(
                "CRITICAL - hypervisor not found or unable to talk to nova. "
                "Error: %s" % e)
            return 2

        # the hypervisor may not have a servers attribute if there are no
        # running instances. This is okay, but we should check another member
        # to make sure we have a valid hypervisor object.
        if hypervisor.hypervisor_hostname == "":
            print("CRITICAL - returned hypervisor object looks corrupt")
            return 2

        end = datetime.utcnow()
        start = end - timedelta(minutes=parsed_args.search_range)

        # select a number of live servers
        servers = []
        if hasattr(hypervisor, 'servers'):
            # pick the server at random
            shuffle(hypervisor.servers)
            for s in hypervisor.servers:
                nova.servers.get(s['uuid']).status
                server_obj = nova.servers.get(s['uuid'])

                if server_obj.status == 'ACTIVE':
                    if len(servers) < parsed_args.num_servers:
                        servers.append(server_obj)
                    else:
                        break

        if not servers:
            print("OK - no active instances on hypervisor, "
                  "not checking for samples.")
            return 0

        criticals = []
        warnings = []

        for server in servers:
            ceilometer = self.get_client('metering')

            fields = {
                'meter_name': 'instance',
                'q': [
                    {'field': 'timestamp', 'op': 'ge', 'value': str(start)},
                    {'field': 'timestamp', 'op': 'lt', 'value': str(end)},
                    {'field': 'resource', 'op': 'eq', 'value': server.id},
                ],
            }

            try:
                samples = ceilometer.samples.list(**fields)
            except Exception as e:
                criticals.append(
                    "unable to get samples from ceilometer for server: %s (%s)"
                    " Error: %s" % (server.name, server.id, e))
                continue

            num_samples = len(samples)

            if num_samples <= parsed_args.critical:
                created_at = datetime.strptime(
                    server_obj.created, "%Y-%m-%dT%H:%M:%SZ")

                if created_at > start:
                    warnings.append(
                        "no recent samples for new server: %s (%s)" %
                        (server.name, server.id))
                else:
                    criticals.append(
                        "no recent samples for server: %s (%s)" %
                        (server.name, server.id))
            elif num_samples < parsed_args.warning:
                warnings.append(
                    "Sample count below threshold for server: %s (%s)" %
                    (server.name, server.id))

        for critical in criticals:
            print("CRITICAL - %s" % critical)

        for warning in warnings:
            print("WARNING - %s" % warning)

        if criticals:
            return 2
        if warnings:
            return 1

        print("OK: Samples collected.")
        return 0


def run():
    CheckCeilometerHypervisorSamples().run()


if __name__ == '__main__':
    run()
