#!/usr/bin/env python
#
# Copyright (c) 2018 Catalyst.net Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may
# not use this file except in compliance with the License. You may obtain
# a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.

from openstack_checks.shared import check


LIST_LIMIT = 9999999999999999999999999


class CheckGlance(check.BaseCheck):
    """
    Check an OpenStack Glance server.
    """

    def get_parser(self, parser):
        glance_args = parser.add_argument_group('Glance check')

        glance_args.add_argument(
            '--min-count', metavar='<min-count>', type=int, default=5,
            required=False, help='minimum number of images in glance')

        glance_args.add_argument(
            '--expected-images', metavar='[expected_images ...]',
            type=str, nargs='+', required=False,
            help='names of images which must be available')

    def take_action(self, parsed_args):
        conn = self.get_connection()

        try:
            # NOTE(adriant) uncomment when the pagination fix is released:
            # image_names = {
            #     i.name for i in conn.image.images(
            #         limit=LIST_LIMIT, visibility='public')
            # }
            # NOTE(adriant) remove the following when fix is released:
            image_names = {
                i['name'] for i in conn.image.get(
                    "images?visibility=public&limit=%s" % LIST_LIMIT
                ).json()['images']
            }
        except Exception as e:
            print(
                "CRITICAL - Failed to get images from Glance. "
                "Error: %s" % e)
            return 2

        errors = []

        if parsed_args.expected_images:
            missing_images = set(parsed_args.expected_images) - image_names
            if missing_images:
                errors.append(
                    "We are missing the following images: %s" %
                    ",".join(missing_images))

        image_count = len(image_names)

        if image_count < parsed_args.min_count:
            errors.append(
                "Expected %s images, found only %s" %
                (parsed_args.min_count, image_count))

        if errors:
            for error in errors:
                print(error)
            return 2
        else:
            print("OK: Glance returned expected images.")
            return 0


def run():
    CheckGlance().run()


if __name__ == '__main__':
    run()
