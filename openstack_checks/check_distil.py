#!/usr/bin/env python
#
# Copyright (c) 2018 Catalyst.net Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may
# not use this file except in compliance with the License. You may obtain
# a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.

from openstack_checks.shared import check


class CheckDistil(check.BaseCheck):
    """
    Nagios check for Distil data collection.
    """

    def take_action(self, parsed_args):
        try:
            # Use openstack client instead of openstacksdk because distil
            # is not supported yet.
            d_client = self.get_client("ratingv2")
            health_data = d_client.health.get()['health']
        except Exception as e:
            print(
                "CRITICAL - Failed to get health data from Distil. "
                "Error: %s" % e)
            return 2

        collection_data = health_data['usage_collection']
        if collection_data['status'] == "OK":
            print("OK: %s" % collection_data['msg'])
            return 0
        else:
            print("CRITICAL - %s" % collection_data['msg'])
            return 2


def run():
    CheckDistil().run()


if __name__ == '__main__':
    run()
